import React, { Component } from 'react';

class Resultado extends Component {

    showResultado = () => {
        const {name, quotes}    = this.props.cotizacion;
        const monedaCotizada    = this.props.monedaCotizada;
        
        if( !name ) return null;
        const {price, percent_change_1h, percent_change_24h} = quotes[ monedaCotizada ];
        console.log( price );
        console.log( percent_change_1h );
        console.log( percent_change_24h );
        return (
            <div className='bg-success py-4'>
                <div className='resumen text-light text-center'>
                    <h2 className='mb4'> Resumen </h2>
                    <p> <span className='font-weight-bold'>  El precio de {name} en {monedaCotizada}: </span> $ {(price).toFixed(2)} </p>
                    <p> <span className='font-weight-bold'>  Porcentaje última hora: </span> {(percent_change_1h).toFixed(2)} % </p>
                    <p> <span className='font-weight-bold'>  Porcentaje últimas 24 horas: </span> {(percent_change_24h).toFixed(2)} % </p>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                { this.showResultado() }
            </div>
        );
    }
}

export default Resultado;