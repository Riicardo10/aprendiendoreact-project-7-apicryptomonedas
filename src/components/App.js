import React, { Component } from 'react';
import Header from './Header';
import Formulario from './Formulario';
import axios from 'axios';
import Resultado from './Resultado';
// http://tobiasahlin.com/spinkit/

class App extends Component {

  state = {
    monedas: [], 
    cotizacion: {},
    monedaCotizada: '',
    cargando: false
  }

  async componentDidMount() {
    this.getMonedas();
  }

  getMonedas = async () => {
    const URL = 'https://api.coinmarketcap.com/v2/ticker/'
    await axios.get( URL )
      .then( res => {
        this.setState( {monedas: res.data.data} );
      } )
  }

  getValoresCrypto = async (moneda) => {
    const URL = 'https://api.coinmarketcap.com/v2/ticker/' + moneda.cryptomoneda + '/?convert=' + moneda.moneda;
    await axios.get( URL )
      .then( res => {
        this.setState( {cargando: true} );
        setTimeout( () => {
          this.setState( {
            cotizacion: res.data.data,
            monedaCotizada: moneda.moneda,
            cargando: false
          } );
        }, 1500);
      } );
  }

  render() {

    const cargando = this.state.cargando;
    let resultado;
    if( cargando ) {
      resultado = <div className="spinner"></div>
    }
    else{
      resultado = <Resultado 
                    cotizacion={this.state.cotizacion}
                    monedaCotizada={this.state.monedaCotizada}/>
    }

    return (
      <div className='container'>
        <Header 
            titulo='Cryptomonedas' />
        <div className='row justify-content-center'>
          <div className='col-md-6 bg-light pb-4 contenido-principal'>
            <Formulario
                monedas={this.state.monedas}
                getValoresCrypto={this.getValoresCrypto}/>
            { resultado }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
