import React, { Component } from 'react';
import FormularioOptionSelect from './FormularioOptionSelect';

class Formulario extends Component {

    moneda       = React.createRef();
    cryptomoneda = React.createRef();

    cotizarMonedas = (e) => {
        e.preventDefault();
        let moneda       = this.moneda.current.value;
        let cryptomoneda = this.cryptomoneda.current.value;
        const cotizacion = {
            moneda,
            cryptomoneda
        }
        this.props.getValoresCrypto( cotizacion );
    }

    render() {
        const monedas = this.props.monedas;
        return (
            <form onSubmit={this.cotizarMonedas}>
                <div className='form-group'>
                    <label> Moneda: </label>
                    <select className='form-control' ref={this.moneda}>
                        <option value='' disabled defaultValue> Elige moneda... </option>
                        <option value='USD' > Dolar USA </option>
                        <option value='MXN' > Peso MX   </option>
                        <option value='EUR' > Euro      </option>
                    </select>
                </div>
                <div className='form-group'>
                    <label> Cryptomoneda: </label>
                    <select className='form-control' ref={this.cryptomoneda}>
                        { Object.keys( monedas ).map( key => {
                            // return <option key={key} value={monedas[key].id}> {monedas[key].name} </option>
                            return <FormularioOptionSelect 
                                        key={key} 
                                        moneda={monedas[key]} />
                        } ) }
                    </select>
                </div>
                <div className='form-group'>
                    <input type='submit' className='btn btn-info' value='Cotizar' />
                </div>
            </form>
        );
    }
}

export default Formulario;